package com.springapp.mvc.forms;


import javax.persistence.*;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
@Entity
@Table(name = "Registration")
public class Registration {
    private Long id;
    private String userName;
    @NotEmpty
    @Size(min = 4, max = 20)
    private String password;
    @NotEmpty
    private String confirmPassword;
    @NotEmpty
    @Email
    private String email;

    public void setUserName(String userName) {
        this.userName = userName;
    }
    @Column(name ="userName")
    public String getUserName() {
        return userName;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    @Column(name ="pd")
    public String getPassword() {
        return password;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }
    @Column(name="confirmPassword")
    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    @Column(name = "email")
    public String getEmail() {
        return email;
    }
    @Id
    @GeneratedValue
    @Column(name="id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}