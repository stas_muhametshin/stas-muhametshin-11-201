package com.springapp.mvc.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Map;

/**
 * Created by Stas on 18.03.14.
 */

@Controller
@RequestMapping("/changepasswordref")
public class ChangepsdRefController {

    @RequestMapping(method = RequestMethod.GET)
    public String showLogIn(Map model){
        return "changepassword";
    }
}
