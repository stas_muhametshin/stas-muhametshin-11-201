package com.springapp.mvc.dbwork;

import com.springapp.mvc.forms.Registration;
import com.springapp.mvc.interfaces.RegistrationDAO;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.hibernate.criterion.Expression;
import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Stas on 14.03.14.
 */
public class RegistrationDaoImp implements RegistrationDAO {



    @Override
    public void addRegistration(Registration registration) {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.save(registration);
            session.getTransaction().commit();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "Ошибка I/O", JOptionPane.OK_OPTION);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    @Override
    public void updateRegistration(Registration registration) {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.update(registration);
            session.getTransaction().commit();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "Ошибка I/O", JOptionPane.OK_OPTION);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    @Override
    public Registration getRegistrationByEmail(String email) {
        Session session = null;
        Registration reg=null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            reg = (Registration)session.createCriteria(Registration.class).add(Expression.eq("email",email)).list().get(0);
            session.getTransaction().commit();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "Ошибка I/O", JOptionPane.OK_OPTION);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return reg;
    }

    @Override
    public List getAllRegistration() {
        Session session = null;
        List<Registration> reg = new ArrayList<Registration>();
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            reg = session.createCriteria(Registration.class).list();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "Ошибка I/O", JOptionPane.OK_OPTION);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return reg;
    }
}
