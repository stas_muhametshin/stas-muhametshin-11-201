package com.springapp.mvc.dbwork;

import com.springapp.mvc.interfaces.RegistrationDAO;

/**
 * Created by Stas on 15.03.14.
 */
public class Factory {
    private static RegistrationDAO registrationDAO = null;
    private static Factory instance = null;
    public static synchronized Factory getInstance(){
        if (instance == null){
            instance = new Factory();
        }
        return instance;
    }
    public RegistrationDAO getRegistrationDAO(){
        if (registrationDAO == null){
            registrationDAO = new RegistrationDaoImp();
        }
        return registrationDAO;
    }
}
