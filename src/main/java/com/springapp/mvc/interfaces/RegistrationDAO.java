package com.springapp.mvc.interfaces;

import com.springapp.mvc.forms.Registration;

import java.util.List;


public interface RegistrationDAO {
    public void addRegistration(Registration registration);
    public void updateRegistration(Registration registration);
    public Registration getRegistrationByEmail(String email);
    public List getAllRegistration();
}
